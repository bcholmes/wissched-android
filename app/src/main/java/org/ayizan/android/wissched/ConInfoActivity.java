package org.ayizan.android.wissched;

import android.os.Bundle;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import org.ayizan.android.event.util.StringUtils;

public class ConInfoActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coninfo_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String resourceName = getIntent().getStringExtra("ASSET_NAME");

        WebView webView = (WebView) findViewById(R.id.bio_details);
        if (resourceName.endsWith(".svg")) {
            webView.setBackgroundColor(0xffffffff);
        } else {
            webView.setBackgroundColor(0x00000000);
        }
        String banner = getIntent().getStringExtra("ASSET_BANNER");
        if (StringUtils.isNotEmpty(banner)) {
            assignBannerImage(banner);
        } else {
            this.<CollapsingToolbarLayout>findViewById(R.id.toolbar_layout).setExpandedTitleTextAppearance(R.style.AppBarExpanded_AlwaysVisible);
        }

        webView.setWebViewClient(new WisSchedWebViewClient(this, null));

        String description = getIntent().getStringExtra("ASSET_DESCRIPTION");
        getSupportActionBar().setTitle(description);
        refreshWebView();
    }

    private void assignBannerImage(String banner) {
        ImageView bannerImageView = this.findViewById(R.id.header_image);
        bannerImageView.setVisibility(View.VISIBLE);
        if ("dessert".equalsIgnoreCase(banner)) {
            bannerImageView.setImageResource(R.mipmap.bg_dessert);
        } else if ("rules".equalsIgnoreCase(banner)) {
            bannerImageView.setImageResource(R.mipmap.bg_rules);
        } else if ("swimming".equalsIgnoreCase(banner)) {
            bannerImageView.setImageResource(R.mipmap.bg_swimming);
        } else if ("parking".equalsIgnoreCase(banner)) {
            bannerImageView.setImageResource(R.mipmap.bg_parking);
        } else if ("communication".equalsIgnoreCase(banner)) {
            bannerImageView.setImageResource(R.mipmap.bg_keep_in_touch);
        }
    }

    protected void refreshWebView() {
        String resourceName = getIntent().getStringExtra("ASSET_NAME");
        WebView webView = (WebView) findViewById(R.id.bio_details);
        webView.loadUrl("file:///android_asset/" + resourceName);
    }

}
