package org.ayizan.android.wissched.view;

import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.ayizan.android.wissched.ItemOpener;
import org.ayizan.android.event.model.Announcement;

import java.util.List;

/**
 * Created by BC Holmes on 2016-08-18.
 */
class AnnouncementViewAdapter extends BaseRecordViewAdapter<Announcement> {

    public AnnouncementViewAdapter(RecyclerView recyclerView, List<Announcement> announcements,
                                   ItemOpener<Announcement> opener) {
        super(recyclerView, announcements, opener);
    }

    @Override
    public BaseRecordViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(android.R.layout.simple_list_item_2, parent, false);
        return new ViewHolder(v, this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Announcement currentItem = this.records.get(position);

        TextView textView = (TextView) holder.view.findViewById(android.R.id.text1);
        textView.setText(Html.fromHtml(currentItem.getHtmlTitle()));
        textView.setSingleLine();
        textView.setEllipsize(TextUtils.TruncateAt.END);

        TextView subLabel = (TextView) holder.view.findViewById(android.R.id.text2);

        subLabel.setText(Html.fromHtml(currentItem.getHtmlSubtitle()));
        subLabel.setSingleLine();
        subLabel.setEllipsize(TextUtils.TruncateAt.END);
    }
}
