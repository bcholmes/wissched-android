package org.ayizan.android.wissched.view.restaurant;

import androidx.recyclerview.widget.SortedList;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.ayizan.android.wissched.ItemOpener;
import org.ayizan.android.wissched.R;
import org.ayizan.android.wissched.service.Restaurant;
import org.ayizan.android.wissched.view.BaseStickyHeaderRecordAdapter;

import java.util.List;

/**
 * Created by BC Holmes on 2016-08-19.
 */
class RestaurantListAdapter extends BaseStickyHeaderRecordAdapter<Restaurant> {

    SortedList<Restaurant> sortedList = new SortedList<Restaurant>(Restaurant.class, new SortedList.Callback<Restaurant>() {
        @Override
        public int compare(Restaurant a, Restaurant b) {
            return a.compareTo(b);
        }

        @Override
        public void onInserted(int position, int count) {
            notifyItemRangeInserted(position, count);
        }

        @Override
        public void onRemoved(int position, int count) {
            notifyItemRangeRemoved(position, count);
        }

        @Override
        public void onMoved(int fromPosition, int toPosition) {
            notifyItemMoved(fromPosition, toPosition);
        }

        @Override
        public void onChanged(int position, int count) {
            notifyItemRangeChanged(position, count);
        }

        @Override
        public boolean areContentsTheSame(Restaurant oldItem, Restaurant newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areItemsTheSame(Restaurant item1, Restaurant item2) {
            return item1 == item2;
        }
    });

    RestaurantListAdapter(RecyclerView recyclerView,
                          List<Restaurant> restaurants,
                          ItemOpener<Restaurant> opener) {
        super(recyclerView, null, opener);
        replaceAll(restaurants);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.restaurant_list_item, parent, false);
        return new ViewHolder(v, this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        View view = holder.getView();

        TextView textView = view.findViewById(R.id.title);
        textView.setSingleLine();
        textView.setEllipsize(TextUtils.TruncateAt.END);

        Restaurant restaurant = getRecord(position);
        textView.setText(restaurant.getName());

        TextView subLabel = view.findViewById(R.id.address);
        subLabel.setText(restaurant.getStreetAddress());
        subLabel.setSingleLine();
        subLabel.setEllipsize(TextUtils.TruncateAt.END);

        TextView cuisine = view.findViewById(R.id.cuisine);
        cuisine.setText(restaurant.getCuisine());
        cuisine.setSingleLine();
        cuisine.setEllipsize(TextUtils.TruncateAt.END);

        TextView priceRange = view.findViewById(R.id.price_range);
        priceRange.setText(restaurant.getPriceRangeString());

        TextView annotations = view.findViewById(R.id.annotations);
        annotations.setText(restaurant.getAnnotations());
        annotations.setSingleLine();
        annotations.setEllipsize(TextUtils.TruncateAt.END);
    }

    @Override
    public int getItemCount() {
        return this.sortedList.size();
    }

    protected Restaurant getRecord(int position) {
        return this.sortedList.get(position);
    }


    public void replaceAll(List<Restaurant> events) {
        this.sortedList.beginBatchedUpdates();
        for (int i = this.sortedList.size()-1; i >=0; i--) {
            Restaurant event = this.sortedList.get(i);
            if (!events.contains(event)) {
                this.sortedList.remove(event);
            }
        }
        this.sortedList.addAll(events);
        this.sortedList.endBatchedUpdates();
        initializeHeaders(events);
    }
}
