package org.ayizan.android.wissched.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.ayizan.android.event.model.SimpleListItem;
import org.ayizan.android.wissched.ItemOpener;
import org.ayizan.android.wissched.R;
import org.ayizan.android.wissched.WisSchedApplication;

import java.util.List;

public class SimpleListActivity extends AppCompatActivity {

    class SimpleListAdapter extends BaseRecordViewAdapter<SimpleListItem> {
        public SimpleListAdapter(RecyclerView recyclerView, List<SimpleListItem> records,
                                 ItemOpener<SimpleListItem> opener) {
            super(recyclerView, records, opener);
        }

        @Override
        public BaseRecordViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                   int viewType) {
            boolean hasSubTitle = false;
            for (SimpleListItem item : this.records) {
                hasSubTitle |= (item.getSubTitle() != null && item.getSubTitle().length() > 0);
                if (hasSubTitle) {
                    break;
                }
            }

            View v = LayoutInflater.from(parent.getContext())
                    .inflate(hasSubTitle ? android.R.layout.simple_list_item_2 : android.R.layout.simple_list_item_1,
                            parent, false);
            return new ViewHolder(v, this);
        }


        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            SimpleListItem item = this.records.get(position);
            TextView textView = (TextView) holder.view.findViewById(android.R.id.text1);
            textView.setText(item.getTitle());
            if (item.getSubTitle() == null || item.getSubTitle().length() == 0) {
                TextView textView2 = (TextView) holder.view.findViewById(android.R.id.text2);
                if (textView2 != null) {
                    textView2.setVisibility(View.GONE);
                }
            } else {
                TextView textView2 = (TextView) holder.view.findViewById(android.R.id.text2);
                textView2.setText(item.getSubTitle());

            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.simple_list_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String listName = getIntent().getStringExtra("ASSET_DESCRIPTION");
        setTitle(listName);

        String listKey = getIntent().getStringExtra("ASSET_NAME");

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.simple_list);
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        final List<SimpleListItem> values = ((WisSchedApplication) getApplication()).getReferenceService().getList(listKey);

        BaseRecordViewAdapter<SimpleListItem> adapter = new SimpleListAdapter(recyclerView, values, new ItemOpener<SimpleListItem>() {
            public void open(SimpleListItem item) {
                if (item.getHref() != null && item.getHref().length() > 0) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getHref()));
                    startActivity(browserIntent);
                }
            }
        });
        recyclerView.setAdapter(adapter);
    }


}
