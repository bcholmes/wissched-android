package org.ayizan.android.wissched;

import androidx.fragment.app.Fragment;

import org.ayizan.android.event.util.WordUtils;
import org.ayizan.android.wissched.view.AnnouncementsFragment;
import org.ayizan.android.wissched.view.LeaderboardFragment;
import org.ayizan.android.wissched.view.MyScheduleFragment;
import org.ayizan.android.wissched.view.ParticipantsFragment;
import org.ayizan.android.wissched.view.ProgramFragment;
import org.ayizan.android.wissched.view.ReferenceListFragment;
import org.ayizan.android.wissched.view.restaurant.RestaurantListFragment;
import org.ayizan.android.wissched.view.grid.GridScheduleFragment;

public enum MenuOption {
	PROGRAM(ProgramFragment.class), MY_SCHEDULE(MyScheduleFragment.class),
	GRID(GridScheduleFragment.class),
	BIOS(ParticipantsFragment.class),
	CON_INFO(ReferenceListFragment.class),
	RESTAURANTS(RestaurantListFragment.class),
	ANNOUNCEMENTS(AnnouncementsFragment.class),
	LEADERBOARD(LeaderboardFragment.class),
	LOGIN(null);

	private Class<? extends Fragment> fragmentClass;
	
	private MenuOption(Class<? extends Fragment> fragmentClass) {
		this.fragmentClass = fragmentClass;
	}
	public String toString() {
		return WordUtils.capitalizeFully(name().replace('_', ' '));
	}
	public Class<? extends Fragment> getFragmentClass() {
		return this.fragmentClass;
	}
}
