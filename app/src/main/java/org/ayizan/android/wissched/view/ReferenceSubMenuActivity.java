package org.ayizan.android.wissched.view;

import org.ayizan.android.wissched.ConInfoActivity;
import org.ayizan.android.wissched.ItemOpener;
import org.ayizan.android.wissched.R;
import org.ayizan.android.wissched.WisSchedApplication;
import org.ayizan.android.wissched.service.ReferenceItem;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

public class ReferenceSubMenuActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.simple_list_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String subMenuName = getIntent().getStringExtra("MENU_NAME");
        setTitle(subMenuName);

        this.recyclerView = (RecyclerView) findViewById(R.id.simple_list);
        this.recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        this.layoutManager = new LinearLayoutManager(this);
        this.recyclerView.setLayoutManager(this.layoutManager);

        final ReferenceItem[] values = ((WisSchedApplication) getApplication()).getReferenceService().getSubMenu(subMenuName);

        this.adapter = new ReferenceItemViewAdapter(this.recyclerView, values, new ItemOpener<ReferenceItem>() {
            public void open(ReferenceItem item) {
                if (item.isSubMenu()) {
                    Intent intent = new Intent(ReferenceSubMenuActivity.this,ReferenceSubMenuActivity.class);
                    intent.putExtra("MENU_NAME", item.getDescription());
                    ReferenceSubMenuActivity.this.startActivity(intent);
                } else {
                    Intent intent = new Intent(ReferenceSubMenuActivity.this,ConInfoActivity.class);
                    intent.putExtra("ASSET_DESCRIPTION", item.getDescription());
                    intent.putExtra("ASSET_NAME", item.getResourceName());
                    ReferenceSubMenuActivity.this.startActivity(intent);
                }
            }
        });
        this.recyclerView.setAdapter(this.adapter);
    }

}
