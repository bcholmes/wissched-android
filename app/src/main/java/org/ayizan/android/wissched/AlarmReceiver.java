package org.ayizan.android.wissched;

import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import android.util.Log;

import org.ayizan.android.event.BaseEventApplication;
import org.ayizan.android.event.model.Event;
import org.ayizan.android.event.service.EventService;
import org.ayizan.android.wissched.view.EventOpener;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(BaseEventApplication.TAG, "AlarmReceiver.onReceive");
        EventService eventService = ((BaseEventApplication) context.getApplicationContext()).getEventService();
        Event event = eventService.getEventById(intent.getExtras().getString(EventOpener.ENTITY_ID));
        if (event != null) {
            Log.i(BaseEventApplication.TAG, "event: " + event.getId());
            Intent onClickIntent = new EventOpener(context).createIntent(event);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addNextIntentWithParentStack(onClickIntent);
            PendingIntent pendingIntent =
                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "org.ayizan.wissched.reminder")
                    .setSmallIcon(R.drawable.ic_notification_icon)
                    .setContentTitle("Event Reminder")
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setContentText(event.getFullTitleNoHtml())
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
            notificationManager.notify(("wissched-event-" + event.getId()).hashCode(), builder.build());
        }
    }
}
