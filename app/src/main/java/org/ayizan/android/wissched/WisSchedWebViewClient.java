package org.ayizan.android.wissched;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.ayizan.android.event.model.EntityType;
import org.ayizan.android.wissched.service.ReferenceItem;
import org.ayizan.android.wissched.service.ReferenceService;
import org.ayizan.android.wissched.view.bio.ParticipantBioActivity;
import org.ayizan.android.wissched.view.event.EventDetailsActivity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.SoundEffectConstants;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

final class WisSchedWebViewClient extends WebViewClient {
	
	static class TrivialHashTagProvider implements HashTagProvider {
		@Override
		public String getHashTag() {
			return "";
		}
	}
	
	private final Activity activity;
	private HashTagProvider hashTagProvider;

	WisSchedWebViewClient(Activity activity, HashTagProvider hashTagProvider) {
		this.activity = activity;
		this.hashTagProvider = hashTagProvider != null ? hashTagProvider : new TrivialHashTagProvider();
	}

	@Override
	public boolean shouldOverrideUrlLoading(WebView view, String url) {
		if (url.contains("/device/")) {
			view.playSoundEffect(SoundEffectConstants.CLICK);


			if (url.contains("/device/info/")) {

				ReferenceService referenceService = ((WisSchedApplication) this.activity.getApplication()).getReferenceService();
				Log.w("WisSched",  "url " + url.substring(url.lastIndexOf('/')));
				ReferenceItem item = referenceService.findReferenceItenByLink(url.substring(url.lastIndexOf('/') + 1));

				if (item != null) {

					Intent intent = new Intent(view.getContext(), ConInfoActivity.class);
					intent.putExtra("ASSET_NAME", item.getResourceName());
					intent.putExtra("ASSET_DESCRIPTION", item.getDescription());
					this.activity.startActivity(intent);
				}
			} else {

				EntityType type = url.contains("/device/event/") ? EntityType.EVENT : EntityType.PARTICIPANT;
				String id = url.substring(url.lastIndexOf("/") + 1);

				Intent intent = new Intent(view.getContext(), type == EntityType.EVENT
						? EventDetailsActivity.class : ParticipantBioActivity.class);
				intent.putExtra("ENTITY_ID", id);
				intent.putExtra("ENTITY_TYPE", type);
				activity.startActivity(intent);
			}
	        
			return true;
		} else if (url.toLowerCase().startsWith("http:") || url.toLowerCase().startsWith("https:") || url.toLowerCase().startsWith("mailto:")) {
			try {
			    Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			    activity.startActivity(myIntent);
			} catch (ActivityNotFoundException e) {
			    Toast.makeText(activity, "Oh dear. Horrible horrible things have happened",  
			    		Toast.LENGTH_LONG).show();
			}
			return true;
		} else if (url.startsWith("geo:")) {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			activity.startActivity(intent);
			return true;
		} else if (url.contains("/tweet/")) {
			try {
				String tweetUrl = "https://twitter.com/intent/tweet?text=" + URLEncoder.encode(this.hashTagProvider.getHashTag(), "UTF-8");
				Uri uri = Uri.parse(tweetUrl);
				activity.startActivity(new Intent(Intent.ACTION_VIEW, uri));
			} catch (UnsupportedEncodingException e) {
			}
			return true;
		} else if (url.startsWith("tel:")) {
			Intent intent = new Intent(Intent.ACTION_DIAL);
			intent.setData(Uri.parse(url));
			this.activity.startActivity(intent);
			return true;
		} else {
			return false;
		}
	}
}