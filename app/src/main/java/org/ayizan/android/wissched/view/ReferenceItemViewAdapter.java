package org.ayizan.android.wissched.view;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.ayizan.android.wissched.ItemOpener;
import org.ayizan.android.wissched.service.ReferenceItem;

import java.util.Arrays;

/**
 * Created by BC Holmes on 2016-08-16.
 */
class ReferenceItemViewAdapter extends BaseRecordViewAdapter<ReferenceItem> {

    public ReferenceItemViewAdapter(RecyclerView recyclerView, ReferenceItem[] myDataset, ItemOpener<ReferenceItem> opener) {
        super(recyclerView, Arrays.asList(myDataset), opener);
    }

    @Override
    public BaseRecordViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                  int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(android.R.layout.simple_list_item_1, parent, false);
        return new ViewHolder(v, this);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TextView textView = (TextView) holder.view.findViewById(android.R.id.text1);
        textView.setText(this.records.get(position).getDescription());
    }
}
