package org.ayizan.android.wissched.view;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import org.ayizan.android.wissched.ConInfoActivity;
import org.ayizan.android.wissched.ItemOpener;
import org.ayizan.android.wissched.service.ReferenceItem;

public class ReferenceItemOpener implements ItemOpener<ReferenceItem> {

    private final Activity activity;

    public ReferenceItemOpener(Activity activity) {
        this.activity = activity;
    }

    public void open(ReferenceItem item) {
        if (item.isFilter()) {
            Log.i("WisSched", "Filter: " + item.getEventFilter().getEventType());
            Intent intent = new Intent(getActivity(), FilteredEventListActivity.class);
            intent.putExtra("ASSET_DESCRIPTION", item.getDescription());
            intent.putExtra("ASSET_FILTER", item.getEventFilter());
            getActivity().startActivity(intent);
        } else if (item.isList()) {
            Intent intent = new Intent(getActivity(), SimpleListActivity.class);
            intent.putExtra("ASSET_DESCRIPTION", item.getDescription());
            intent.putExtra("ASSET_NAME", item.getResourceName());
            getActivity().startActivity(intent);
        } else if (item.isSubMenu()) {
            Intent intent = new Intent(getActivity(),ReferenceSubMenuActivity.class);
            intent.putExtra("MENU_NAME", item.getDescription());
            getActivity().startActivity(intent);
        } else {
            Intent intent = new Intent(getActivity(),ConInfoActivity.class);
            intent.putExtra("ASSET_DESCRIPTION", item.getDescription());
            intent.putExtra("ASSET_NAME", item.getResourceName());
            if (item.getBanner() != null) {
                intent.putExtra("ASSET_BANNER", item.getBanner());
            }
            getActivity().startActivity(intent);
        }
    }

    public Activity getActivity() {
        return activity;
    }
}