package org.ayizan.android.wissched.service;


import android.content.Context;

import org.ayizan.android.event.model.SimpleListItem;
import org.ayizan.android.event.util.JSONResourceReader;
import org.ayizan.android.event.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReferenceService {

	private Context context;
	private JSONResourceReader reader = new JSONResourceReader();


	public ReferenceService(Context context) {
		this.context = context;
	}

	public ReferenceItem[] getReferenceItems() {
		try {
			JSONObject json = this.reader.loadJson(this.context, "wisschedConInfo.json");
			JSONArray array = json.getJSONArray("content");

			return parseItemList(array);
		} catch (JSONException e) {
			return new ReferenceItem[0];
		}
	}

	public ReferenceItem getReferenceItemByKey(String key) {
		ReferenceItem result = null;
		for (ReferenceItem item: getReferenceItems()) {
			if (key != null && key.equalsIgnoreCase(item.getKey())) {
				result = item;
				break;
			}
		}
		return result;
	}

	private ReferenceItem[] parseItemList(JSONArray array) throws JSONException {
		List<ReferenceItem> result = new ArrayList<>();
		for (int i = 0; i < array.length(); i++) {
			JSONObject record = array.getJSONObject(i);
			if (record.has("filter")) {
				JSONObject filter = record.getJSONObject("filter");
				result.add(new ReferenceItem(record.getString("title"), new EventFilter(filter.getString("eventType"))));
			} else if (record.has("resource")) {
				ReferenceItem item = new ReferenceItem(record.getString("title"), record.getString("resource"));
				if (record.has("banner")) {
					item.setBanner(record.getString("banner"));
				}
				result.add(item);
			} else if (record.has("list")) {
				result.add(ReferenceItem.createList(record.getString("title"), record.getString("list")));
			} else if (record.has("content")) {
				ReferenceItem[] subItems = parseItemList(record.getJSONArray("content"));
				result.add(new ReferenceItem(record.getString("title"), subItems));
			}

		}
		return result.toArray(new ReferenceItem[0]);
	}

	public ReferenceItem findReferenceItenByLink(String link) {
		ReferenceItem result = null;
		for (ReferenceItem item : getReferenceItems()) {
			if (StringUtils.isNotEmpty(link) && link.equals(item.getResourceName())) {
				result = item;
				break;
			}
		}
		return result;
	}


	public ReferenceItem[] getSubMenu(String subMenuName) {
		ReferenceItem[] result = null;
		ReferenceItem[] items = getReferenceItems();
		for (ReferenceItem referenceItem : items) {
			if (referenceItem.getDescription().equals(subMenuName) && referenceItem.isSubMenu()) {
				result = referenceItem.getItems();
				break;
			}
		}
		return result;
	}

	public List<SimpleListItem> getList(String listKey) {
		try {
			JSONArray array = this.reader.loadJsonArray(this.context, listKey);

			return parseSimpleItemList(array);
		} catch (JSONException e) {
			return Collections.emptyList();
		}

	}

	private List<SimpleListItem> parseSimpleItemList(JSONArray array) throws JSONException {
		List<SimpleListItem> result = new ArrayList<>();
		for (int i = 0; i < array.length(); i++) {
			JSONObject record = array.getJSONObject(i);
			SimpleListItem item = new SimpleListItem();
			if (record.has("href")) {
				item.setHref(record.getString("href"));
			}
			if (record.has("title")) {
				item.setTitle(record.getString("title"));
			}
			if (record.has("subTitle")) {
				item.setSubTitle(record.getString("subTitle"));
			}
			result.add(item);
		}
		return result;
	}
}
