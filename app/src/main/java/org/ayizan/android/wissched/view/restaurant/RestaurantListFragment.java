package org.ayizan.android.wissched.view.restaurant;

import java.util.ArrayList;
import java.util.List;

import org.ayizan.android.wissched.ItemOpener;
import org.ayizan.android.wissched.R;
import org.ayizan.android.wissched.WisSchedApplication;
import org.ayizan.android.event.model.EntityType;
import org.ayizan.android.wissched.service.Restaurant;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import ca.barrenechea.widget.recyclerview.decoration.DividerDecoration;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration;

public class RestaurantListFragment extends Fragment implements SearchView.OnQueryTextListener, OnMapReadyCallback {

	private RecyclerView recyclerView;
	private RestaurantListAdapter adapter;

	SearchView searchView;
	StickyHeaderDecoration headerDecoration;
	private MapView mapView;
	private GoogleMap googleMap;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.restaurant_list, container, false);
		this.recyclerView = (RecyclerView) view.findViewById(R.id.simple_list);

		Bundle mapViewBundle = null;
		if (savedInstanceState != null) {
			mapViewBundle = savedInstanceState.getBundle(getMapKey());
		}

		mapView = view.findViewById(R.id.map_view);
		mapView.onCreate(mapViewBundle);
		mapView.getMapAsync(this);



		return view;
	}

	public String getMapKey() {
		try {
			ApplicationInfo info = getActivity().getPackageManager().getApplicationInfo(getActivity().getPackageName(), PackageManager.GET_META_DATA);
			Bundle bundle = info.metaData;
			return bundle.getString("com.google.android.geo.API_KEY");
		} catch (PackageManager.NameNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_search, menu);

		MenuItem searchItem = menu.findItem(R.id.search);
		this.searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
		this.searchView.setOnQueryTextListener(this);
	}

	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

		final DividerDecoration divider = new DividerDecoration.Builder(this.getActivity())
				.setHeight(R.dimen.default_divider_height)
				.setPadding(R.dimen.default_divider_padding)
				.setColorResource(R.color.lightGray)
				.build();

		this.recyclerView.setHasFixedSize(true);
		this.recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
		this.recyclerView.addItemDecoration(divider);

		List<Restaurant> restaurants = getRestaurants();

        this.adapter = new RestaurantListAdapter(this.recyclerView, restaurants, new ItemOpener<Restaurant>() {
			@Override
			public void open(Restaurant item) {
				Intent intent = new Intent(getActivity(), RestaurantDetailsActivity.class);
				intent.putExtra("ENTITY_ID", item.getId());
				intent.putExtra("ENTITY_TYPE", EntityType.RESTAURANT);
				startActivity(intent);
			}
		});
		this.headerDecoration = new StickyHeaderDecoration(adapter);
		setHasOptionsMenu(true);

		this.recyclerView.setAdapter(this.adapter);
		this.recyclerView.addItemDecoration(this.headerDecoration, 1);

    }

	private List<Restaurant> getRestaurants() {
		WisSchedApplication application = (WisSchedApplication) getActivity().getApplication();
		return application.getRestaurantService().getRestaurants();
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		return false;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		Log.i("WisSched", newText);
		if (newText != null && newText.length() > 0) {
			List<Restaurant> restaurants = filteredRestaurants(newText);
			this.adapter.replaceAll(restaurants);
			addAllMarkers(restaurants);
			this.headerDecoration.clearHeaderCache();
		} else {
			List<Restaurant> restaurants = getRestaurants();
			this.adapter.replaceAll(restaurants);
			addAllMarkers(restaurants);
			this.headerDecoration.clearHeaderCache();
		}
		return false;
	}

	List<Restaurant> filteredRestaurants(String searchTerm) {
		List<Restaurant> result = new ArrayList<>();
		for (Restaurant r : getRestaurants()) {
			if (r.matchesString(searchTerm)) {
				result.add(r);
			}
		}
		return result;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		Bundle mapViewBundle = outState.getBundle(getMapKey());
		if (mapViewBundle == null) {
			mapViewBundle = new Bundle();
			outState.putBundle(getMapKey(), mapViewBundle);
		}

		mapView.onSaveInstanceState(mapViewBundle);
	}
	@Override
	public void onResume() {
		super.onResume();
		mapView.onResume();
	}

	@Override
	public void onStart() {
		super.onStart();
		mapView.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
		mapView.onStop();
	}
	@Override
	public void onPause() {
		mapView.onPause();
		super.onPause();
	}
	@Override
	public void onDestroy() {
		mapView.onDestroy();
		super.onDestroy();
	}
	@Override
	public void onLowMemory() {
		super.onLowMemory();
		mapView.onLowMemory();
	}
	@Override
	public void onMapReady(GoogleMap googleMap) {
		this.googleMap = googleMap;
		addAllMarkers(getRestaurants());
		googleMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(43.075613, -89.386584), 14.0f));
	}

	private void addAllMarkers(List<Restaurant> restaurants) {
		this.googleMap.clear();
		for (Restaurant restaurant: restaurants) {
			addMarker(restaurant);
		}
	}

	void addMarker(Restaurant restaurant) {
		LatLng marker = new LatLng(restaurant.getLatitude(), restaurant.getLongitude());
		googleMap.addMarker(new MarkerOptions().position(marker)
				.icon(new MapMarkerUtil().getMarkerIcon(getActivity()))
				.title(restaurant.getName()));

	}
}
