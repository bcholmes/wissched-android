package org.ayizan.android.wissched.view.restaurant;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.ayizan.android.wissched.R;
import org.ayizan.android.wissched.WisSchedApplication;
import org.ayizan.android.wissched.service.Restaurant;

public class RestaurantDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static final float ZOOM_LEVEL = 16.0f;
    private MapView mapView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_details);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(getMapKey());
        }

        mapView = findViewById(R.id.map_view);
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);

        populateData();
    }

    public String getMapKey() {
        try {
            ApplicationInfo info = getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = info.metaData;
            return bundle.getString("com.google.android.geo.API_KEY");
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private void populateData() {
        final Restaurant restaurant = getRestaurant();

        TextView title = findViewById(R.id.title);
        title.setText(restaurant.getName());

        TextView address = findViewById(R.id.address);
        address.setText(restaurant.getStreetAddress());

        View phoneNumberBlock = findViewById(R.id.phone_number_block);
        if (restaurant.getPhoneNumber() == null || restaurant.getPhoneNumber().length() == 0) {
            phoneNumberBlock.setVisibility(View.GONE);
        } else {
            phoneNumberBlock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + restaurant.getFullyQualifiedPhoneNumber()));
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(RestaurantDetailsActivity.this, "Whoopsie-doodle. Something wrong happened.", Toast.LENGTH_SHORT);
                    }
                }
            });
            TextView phoneNumber = findViewById(R.id.phone_number);
            phoneNumber.setText(restaurant.getPhoneNumber());
        }

        View urlBlock = findViewById(R.id.url_block);
        if (restaurant.getUrl() == null || restaurant.getUrl().length() == 0) {
            urlBlock.setVisibility(View.GONE);
        } else {
            urlBlock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(restaurant.getFullyQualifiedUrl()));
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(RestaurantDetailsActivity.this, "Whoopsie-doodle. Something wrong happened.", Toast.LENGTH_SHORT);
                    }
                }
            });
            TextView url = findViewById(R.id.url);
            url.setText(restaurant.getUrl());
        }
        TextView hours = findViewById(R.id.hours);
        hours.setText(restaurant.getHours());

        TextView cuisine = findViewById(R.id.cuisine);
        cuisine.setText(restaurant.getCuisine());


        TextView priceRange = findViewById(R.id.price_range);
        priceRange.setText(restaurant.getPriceRangeString());

        TextView alcohol = findViewById(R.id.alcohol);
        alcohol.setText(restaurant.getAlcohol());

        TextView veggie = findViewById(R.id.veggie);
        veggie.setText(restaurant.getVeggie());

        TextView allergen = findViewById(R.id.allergen);
        allergen.setText(restaurant.getAllergen());

        TextView takeOut = findViewById(R.id.take_out);
        takeOut.setText(restaurant.getTakeout());

        TextView delivery = findViewById(R.id.delivery);
        delivery.setText(restaurant.getDelivery().toString());

        TextView wifi = findViewById(R.id.wifi);
        wifi.setText(restaurant.getWifi().toString());

        TextView reservations = findViewById(R.id.reservations_recommended);
        reservations.setText(restaurant.getReservationRecommended().toString());

        TextView memorialDay = findViewById(R.id.memorial_day);
        memorialDay.setText(restaurant.getMemorialDayStatus());
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_restaurant_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Restaurant restaurant = getRestaurant();
        switch (item.getItemId()) {
        case R.id.directions:
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps/dir/?daddr=" + restaurant.getLatitude() + "," + restaurant.getLongitude()));
            startActivity(intent);
            break;
        default:
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    public Restaurant getRestaurant() {
        WisSchedApplication application = (WisSchedApplication) getApplication();
        String entityId = getIntent().getStringExtra("ENTITY_ID");
        return application.getRestaurantService().getRestaurantById(entityId);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(getMapKey());
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(getMapKey(), mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }
    @Override
    protected void onPause() {
        mapView.onPause();
        super.onPause();
    }
    @Override
    protected void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Restaurant restaurant = getRestaurant();
        LatLng marker = new LatLng(restaurant.getLatitude(), restaurant.getLongitude());
        googleMap.addMarker(new MarkerOptions().position(marker)
                .icon(new MapMarkerUtil().getMarkerIcon(this))
                .title(restaurant.getName()));
        googleMap.moveCamera( CameraUpdateFactory.newLatLngZoom(marker, ZOOM_LEVEL));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
