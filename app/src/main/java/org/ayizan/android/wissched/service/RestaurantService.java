package org.ayizan.android.wissched.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ayizan.android.event.model.Renderable;
import org.ayizan.android.event.util.JSONResourceReader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class RestaurantService {

	private JSONResourceReader reader = new JSONResourceReader();
	private boolean initialized = false;
	private Map<String,Restaurant> restaurants = new HashMap<String,Restaurant>();
	private Context context;

	public RestaurantService(Context context) {
		this.context = context;
	}

	protected synchronized void initialize() {
		if (!this.initialized) {
			try {
				JSONObject json = this.reader.loadJson(this.context, "restaurants.json");
				
				JSONArray list = json.getJSONArray("restaurants");
				for (int i = 0; i < list.length(); i++) {
					JSONObject restaurantJson = list.getJSONObject(i);
					Restaurant restaurant = Restaurant.fromJson(restaurantJson);
					this.restaurants.put(restaurant.getId(), restaurant);
					
				}
				this.initialized = true;
			} catch (JSONException e) {
				Log.w("WisSched", "JSON restaurant file parse error", e);
			}
		}
	}

	public List<Restaurant> getRestaurants() {
		if (!this.initialized) {
			initialize();
		}
		List<Restaurant> restaurants = new ArrayList<Restaurant>(this.restaurants.values());
		Collections.sort(restaurants);
		return restaurants;
	}

	public Restaurant getRestaurantById(String entityId) {
		return this.restaurants.get(entityId);
	}
}
