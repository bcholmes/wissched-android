package org.ayizan.android.wissched.service;

/**
 * Created by holmesbc on 2016-08-16.
 */
public interface Describable {
    public String getDescription();
}
