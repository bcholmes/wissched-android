package org.ayizan.android.wissched.view;

import org.ayizan.android.event.model.Event;

import java.util.List;

public abstract class EventListFragment extends BaseEventListFragment {


	protected List<Event> getEvents() {
		return getEventService().getLeafProgramItems();
	}

}
