package org.ayizan.android.wissched.view;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;

import org.ayizan.android.wissched.R;
import org.ayizan.android.wissched.service.EventFilter;

/**
 * Created by bcholmes on 2017-05-19.
 */

public class FilteredEventListActivity extends AppCompatActivity {

    EventFilter filter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.basic_activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Log.i("WisSched", "Do the thing" + bundle.getString("ASSET_DESCRIPTION"));
            setTitle(bundle.getString("ASSET_DESCRIPTION"));
            this.filter = (EventFilter) bundle.getSerializable("ASSET_FILTER");

            Fragment fragment = new FilteredEventListFragment();
            fragment.setArguments(bundle);
            displayFragment(fragment);
        } else {
            Log.i("WisSched", "no bundle. Sadness");
        }
    }

    private void displayFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

}