package org.ayizan.android.wissched.service;

import android.os.AsyncTask;
import android.util.Log;

import org.ayizan.android.event.model.Announcement;
import org.ayizan.android.event.service.rest.FetchAnnouncementsResponse;
import org.ayizan.android.event.service.rest.RestService;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class AnnouncementService {

	private RestService restService;
	private List<Announcement> announcements = Collections.<Announcement>emptyList();

	class AnnouncementFetchTask extends AsyncTask<String, Void, Void> {

		private RestService restService;

		protected AnnouncementFetchTask(RestService restService) {
			this.restService = restService;
		}

		@Override
		protected Void doInBackground(String... params) {
			try {
				Log.d("WisSched", "Fetch annouoncements");
				FetchAnnouncementsResponse response = this.restService.fetchAnnouncements();
		        if (response != null) {
		        	AnnouncementService.this.announcements = response.getAnnouncements();
		        }
		        return null;
			} catch (IOException e) {
				Log.e("WisSched", "Unable to download announcements", e);
				return null;
			}
		}
	}
	
	public AnnouncementService(RestService restService) {
		this.restService = restService;
	}

	public List<Announcement> getAnnouncements() {
		return announcements;
	}

	public void fetchLatestUpdates() {
		new AnnouncementFetchTask(this.restService).execute();
	}

	public Announcement getAnnouncementById(String entityId) {
		Announcement result = null;
		for (Announcement announcement : this.announcements) {
			if (entityId.equals(announcement.getId())) {
				result = announcement;
				break;
			}
		}
		return result;
	}
}
