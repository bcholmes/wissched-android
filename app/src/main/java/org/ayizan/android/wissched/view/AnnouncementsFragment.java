package org.ayizan.android.wissched.view;

import java.util.List;

import org.ayizan.android.wissched.AnnouncementActivity;
import org.ayizan.android.wissched.ItemOpener;
import org.ayizan.android.wissched.R;
import org.ayizan.android.wissched.WisSchedApplication;
import org.ayizan.android.event.model.Announcement;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class AnnouncementsFragment extends Fragment {

	private RecyclerView recyclerView;
	private RecyclerView.Adapter adapter;
	private RecyclerView.LayoutManager layoutManager;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.simple_recycler_layout, container, false);
	}

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.recyclerView = (RecyclerView) getView().findViewById(R.id.simple_list);
		this.recyclerView.setHasFixedSize(true);

		this.layoutManager = new LinearLayoutManager(getActivity());
		this.recyclerView.setLayoutManager(this.layoutManager);

        WisSchedApplication application = (WisSchedApplication) getActivity().getApplication();
        final List<Announcement> values = application.getAnnouncementService().getAnnouncements();

		this.adapter = new AnnouncementViewAdapter(this.recyclerView, values, new ItemOpener<Announcement>() {
			public void open(Announcement item) {
				Intent intent = new Intent(getActivity(), AnnouncementActivity.class);
				intent.putExtra("ENTITY_ID", item.getId());
				intent.putExtra("ENTITY_TYPE", item.getEntityType());
				getActivity().startActivity(intent);
			}
		});
		this.recyclerView.setAdapter(this.adapter);

    }
}
