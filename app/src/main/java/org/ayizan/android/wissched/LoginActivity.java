package org.ayizan.android.wissched;

import org.ayizan.android.wissched.service.LoginResult;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity implements LoginResult {

	private EditText username = null;
	private EditText password = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		this.username = (EditText) findViewById(R.id.editText1);
		this.password = (EditText) findViewById(R.id.editText2);
	}

	public void login(View view) {
		WisSchedApplication application = (WisSchedApplication) getApplication();
		application.login(this.username.getText().toString(), this.password.getText().toString(), this);
	}

	@Override
	public void loginSuccess() {
		finish();
		WisSchedApplication application = (WisSchedApplication) getApplication();
		application.fetchAssignments();
	}

	@Override
	public void loginFailed() {
		Toast.makeText(getApplicationContext(), "Nope, nope, nope, nope, nope.",
				Toast.LENGTH_SHORT).show();
	}
}
