package org.ayizan.android.wissched.view;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.ayizan.android.event.service.EventService;
import org.ayizan.android.wissched.R;
import org.ayizan.android.wissched.WisSchedApplication;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import ca.barrenechea.widget.recyclerview.decoration.DividerDecoration;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration;

public class ParticipantsFragment extends Fragment implements PropertyChangeListener  {
	
	private RecyclerView recyclerView;
	private ParticipantListAdapter adapter;
	private FirebaseAnalytics firebaseAnalytics;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.simple_recycler_layout, container, false);
		this.recyclerView = (RecyclerView) view.findViewById(R.id.simple_list);
		this.firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		Bundle bundle = new Bundle();
		bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Participants List");
		this.firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM, bundle);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		final DividerDecoration divider = new DividerDecoration.Builder(this.getActivity())
				.setHeight(R.dimen.default_divider_height)
				.setPadding(R.dimen.default_divider_padding)
				.setColorResource(R.color.lightGray)
				.build();

		this.recyclerView.setHasFixedSize(true);
		this.recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
		this.recyclerView.addItemDecoration(divider);

		setAdapterAndDecoration(this.recyclerView);
	}

	protected void setAdapterAndDecoration(RecyclerView list) {
		this.adapter = new ParticipantListAdapter(this.getActivity(),
				this.recyclerView, getWisSchedApplication().getImageService(),
				getEventService().getParticipants(), new ParticipantItemOpener(getActivity()));
		StickyHeaderDecoration decoration = new StickyHeaderDecoration(adapter);
		setHasOptionsMenu(true);

		list.setAdapter(this.adapter);
		list.addItemDecoration(decoration, 1);
	}

	private EventService getEventService() {
		WisSchedApplication application = getWisSchedApplication();
		return application.getEventService();
	}

	private WisSchedApplication getWisSchedApplication() {
		return (WisSchedApplication) getActivity().getApplication();
	}
	
	@Override
	public void onDestroy() {
		getEventService().removePropertyChangeListener(this);
		super.onDestroy();
	}

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if ("events".equals(event.getPropertyName())) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					ParticipantsFragment.this.adapter.setParticipants(getEventService().getParticipants());
					ParticipantsFragment.this.adapter.notifyDataSetChanged();
				}
			});
		}
	}
}
