package org.ayizan.android.wissched;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

public class WisSchedMainActivity extends AppCompatActivity implements PropertyChangeListener,
        NavigationView.OnNavigationItemSelectedListener {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private DrawerLayout drawerLayout;
	private ActionBarDrawerToggle drawerToggle;

	private List<MenuOption> menuOptions;

    private MenuOption currentOption;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        WisSchedApplication application = (WisSchedApplication) getApplication();
        application.addPropertyChangeListener("loginState", this);
        this.drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // set a custom shadow that overlays the main content when the drawer opens
        drawerLayout.setDrawerShadow(R.mipmap.drawer_shadow, GravityCompat.START);
        populateDrawer();


        // enable ActionBar app icon to behave as action to toggle nav drawer
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        this.drawerToggle = new ActionBarDrawerToggle(
                this, this.drawerLayout, (Toolbar) findViewById(R.id.toolbar),
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(this.drawerToggle);
        this.drawerToggle.syncState();

        if (savedInstanceState == null) {
            selectItem(MenuOption.PROGRAM);
        }
    }

	private void populateDrawer() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        WisSchedApplication application = (WisSchedApplication) getApplication();
        if (application.isLoggedIn()) {
            navigationView.getMenu().findItem(R.id.nav_login).setVisible(false);
        } else {
            navigationView.getMenu().findItem(R.id.nav_login).setVisible(true);
        }
	}

    private void selectItem(MenuOption option) {
        if (option == MenuOption.LOGIN) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        } else {
            Fragment fragment = chooseFragment(option);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
            this.currentOption = option;
            invalidateOptionsMenu();
            setTitle(option.toString());
        }
    }

	private Fragment chooseFragment(MenuOption option) {
		Class<? extends Fragment> fragmentClass = option.getFragmentClass();
		try {
			return fragmentClass.newInstance();
		} catch (InstantiationException e) {
			throw new RuntimeException("Fragment instantiation exception", e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException("Fragment instantiation exception", e);
		}
	}
    
    @Override
    public void setTitle(CharSequence title) {
        getSupportActionBar().setTitle(title);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    
    @Override
    protected void onResume() {
    	super.onResume();
    	WisSchedApplication application = (WisSchedApplication) getApplication();
    	application.performDataSynchIfApplicable();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_program) {
            selectItem(MenuOption.PROGRAM);
        } else if (id == R.id.nav_my_schedule) {
            selectItem(MenuOption.MY_SCHEDULE);
        } else if (id == R.id.nav_con_info) {
            selectItem(MenuOption.CON_INFO);
        } else if (id == R.id.nav_grid) {
            selectItem(MenuOption.GRID);
        } else if (id == R.id.nav_bios) {
            selectItem(MenuOption.BIOS);
        } else if (id == R.id.nav_announcements) {
            selectItem(MenuOption.ANNOUNCEMENTS);
        } else if (id == R.id.nav_restaurants) {
            selectItem(MenuOption.RESTAURANTS);
        } else if (id == R.id.nav_leaderboard) {
            selectItem(MenuOption.LEADERBOARD);
        } else if (id == R.id.nav_login) {
            selectItem(MenuOption.LOGIN);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

	@Override
	public void propertyChange(PropertyChangeEvent event) {
		if ("loginState".equals(event.getPropertyName())) {
			runOnUiThread(new Runnable() {
			     @Override
			     public void run() {
			    	 populateDrawer();
			     }
			});
		}
	}
}
