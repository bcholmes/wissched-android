package org.ayizan.android.wissched.service;

public interface LoginResult {

	public void loginSuccess();
	public void loginFailed();
	
}
