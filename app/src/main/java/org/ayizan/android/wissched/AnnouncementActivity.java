package org.ayizan.android.wissched;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.ayizan.android.event.model.EntityType;
import org.ayizan.android.event.model.Renderable;
import org.ayizan.android.event.util.RenderContext;

public class AnnouncementActivity extends AppCompatActivity implements RenderContext, HashTagProvider {

    protected FirebaseAnalytics firebaseAnalytics;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentLayout());

        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        */

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        WebView webView = findViewById(R.id.bio_details);
        webView.setBackgroundColor(0x00000000);

        findViewById(R.id.header_image).setVisibility(View.VISIBLE);

        webView.setWebViewClient(new WisSchedWebViewClient(this, this));
        refreshWebView();
        this.firebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    protected int getContentLayout() {
        return R.layout.activity_coninfo_details;
    }

    public String getHashTag() {
        return "";
    }

    public Renderable getRenderable() {
        WisSchedApplication application = (WisSchedApplication) getApplication();
        String entityId = getIntent().getStringExtra("ENTITY_ID");
        return application.getAnnouncementService().getAnnouncementById(entityId);
    }

    protected void refreshWebView() {
        WebView webView = findViewById(R.id.bio_details);
        String participantId = getIntent().getStringExtra("ENTITY_ID");
        EntityType entityType = (EntityType) getIntent().getSerializableExtra("ENTITY_TYPE");
        Renderable renderable = getRenderable();

        webView.loadDataWithBaseURL("file:///android_asset/details_" + entityType.name().toLowerCase()
                + "_" + participantId, renderable.getFullHtml(this), "text/html", "UTF-8", "");
    }

    public boolean isImageAvailable() {
        return false;
    }
    public String getImageSource() {
        return null;
    }

    public boolean isAddImageAllowed() {
        return false;
    }

    public String getPersonId() {
        WisSchedApplication application = (WisSchedApplication) getApplication();
        return application.getPersonId();
    }
}
