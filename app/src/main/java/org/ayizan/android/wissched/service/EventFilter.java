package org.ayizan.android.wissched.service;

import org.ayizan.android.event.model.Event;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bcholmes on 2017-05-19.
 */
public class EventFilter implements Serializable {
    private String eventType;

    public EventFilter(String eventType) {
        this.eventType = eventType;
    }

    public String getEventType() {
        return eventType;
    }

    public List<Event> apply(List<Event> events) {
        List<Event> result = new ArrayList<>();

        for (Event event: events) {
            if (this.eventType.equalsIgnoreCase(event.getType())) {
                result.add(event);
            }
        }

        return result;
    }
}
