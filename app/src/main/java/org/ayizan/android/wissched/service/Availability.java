package org.ayizan.android.wissched.service;

import org.ayizan.android.event.util.WordUtils;

public enum Availability {
    YES, NO, UNKNOWN;

    public String toString() {
        return WordUtils.capitalizeFully(name().toLowerCase());
    }
}
