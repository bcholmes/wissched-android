package org.ayizan.android.wissched.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.ayizan.android.event.model.Event;
import org.ayizan.android.wissched.service.EventFilter;

import java.util.List;

/**
 * Created by bcholmes on 2017-05-19.
 */

public class FilteredEventListFragment extends BaseEventListFragment {

    EventFilter filter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.filter = (EventFilter) arguments.getSerializable("ASSET_FILTER");
        }

        View result = super.onCreateView(inflater, container, savedInstanceState);

        return result;
    }

    protected List<Event> getEvents() {
        return this.filter.apply(getEventService().getLeafProgramItems());
    }
}
