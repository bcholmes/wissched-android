package org.ayizan.android.wissched.view;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.ayizan.android.wissched.R;
import org.ayizan.android.wissched.WisSchedApplication;
import org.ayizan.android.wissched.service.ReferenceItem;
import org.ayizan.android.wissched.service.ReferenceService;

public class ReferenceListFragment extends Fragment {

    private RecyclerView recyclerView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.simple_recycler_layout, container, false);
	}
	
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.recyclerView = (RecyclerView) getView().findViewById(R.id.simple_list);
        this.recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        this.recyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        final ReferenceService referenceService = ((WisSchedApplication) getActivity().getApplication()).getReferenceService();
        final ReferenceItem[] values = referenceService.getReferenceItems();

        RecyclerView.Adapter adapter = new ReferenceItemViewAdapter(this.recyclerView, values, new ReferenceItemOpener(getActivity()));
        this.recyclerView.setAdapter(adapter);
    }
}