package org.ayizan.android.wissched.service;

public class ReferenceItem implements Describable {
	
	private String description;
	private String resourceName;
	private ReferenceItem[] items;
	private EventFilter filter;
	private boolean isList;
	private String banner;

	public ReferenceItem(String description, String resourceName) {
		this.description = description;
		this.resourceName = resourceName;
	}
	public ReferenceItem(String description, EventFilter filter) {
		this.description = description;
		this.filter = filter;
	}
	public ReferenceItem(String description, ReferenceItem[] items) {
		this.description = description;
		this.items = items;
	}
	public String getDescription() {
		return description;
	}
	public String getResourceName() {
		return resourceName;
	}
	public String toString() {
		return getDescription();
	}
	public boolean isSubMenu() {
		return this.items != null;
	}
	public boolean isFilter() {
		return this.filter != null;
	}
	public ReferenceItem[] getItems() {
		return items;
	}
	public EventFilter getEventFilter() {
		return this.filter;
	}

    public static ReferenceItem createList(String title, String list) {
		ReferenceItem result = new ReferenceItem(title, list);
		result.isList = true;
		return result;
    }

	public boolean isList() {
		return isList;
	}

	public String getKey() {
		if (this.resourceName != null) {
			return this.resourceName.contains(".")
					? this.resourceName.substring(0, this.resourceName.lastIndexOf('.'))
					: this.resourceName;
		} else {
			return null;
		}
	}

    public void setBanner(String banner) {
		this.banner = banner;
	}

	public String getBanner() {
		return banner;
	}
}
