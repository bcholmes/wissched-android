package org.ayizan.android.wissched;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Base64;

import org.ayizan.android.event.BaseEventApplication;
import org.ayizan.android.event.service.AvatarDownloadTask;
import org.ayizan.android.event.service.ImageService;
import org.ayizan.android.event.service.TokenGenerator;
import org.ayizan.android.event.util.ConnectivityUtil;
import org.ayizan.android.event.util.DateFlyweight;
import org.ayizan.android.event.util.PersonIdProvider;
import org.ayizan.android.event.util.StringUtils;
import org.ayizan.android.wissched.service.AnnouncementService;
import org.ayizan.android.wissched.service.LoginResult;
import org.ayizan.android.wissched.service.ReferenceService;
import org.ayizan.android.wissched.service.RestaurantService;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.TimeZone;
import java.util.UUID;

public class WisSchedApplication extends BaseEventApplication implements PersonIdProvider, TokenGenerator {

	private static final String TOKEN = "hereistherootoftherootandthebudofthebudandtheskyoftheskyofatreecalledlife";

	public static String CURRENT_CONVENTION = "wiscon43";
	public static String DATABASE_NAME = "wiscon.db";

	private static final long THREE_HOURS = 3L * 60L * 60L * 1000L;
	private static final long SIXTY_MINUTES = 60L * 60L * 1000L;
	
	private long assignmentsCheck = -1;
	PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
	private RestaurantService restaurantService = new RestaurantService(this);
	
	ImageService imageService;
	private AnnouncementService announcementService;
	private ReferenceService referenceService;

	long lastDataSynchTime = -1;

	public WisSchedApplication() {
		super("WisSched", CURRENT_CONVENTION, DATABASE_NAME);
		DateFlyweight.timeZone = TimeZone.getTimeZone("America/Chicago");
	}

	@Override
	public void onCreate() {
		super.onCreate();

        this.imageService = new ImageService(getApplicationContext(), this, getRestService());
		this.referenceService = new ReferenceService(this);
        this.announcementService = new AnnouncementService(getRestService());

        unloginIfNecessary();
        createNotificationChannel();
	}

	private void createNotificationChannel() {
		// Create the NotificationChannel, but only on API 26+ because
		// the NotificationChannel class is new and not in the support library
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationChannel channel = new NotificationChannel("org.ayizan.wissched.reminder",
					"WisSched Reminders", NotificationManager.IMPORTANCE_DEFAULT);
			channel.setDescription("A channel for setting notification reminders about event start times");
			// Register the channel with the system; you can't change the importance
			// or other notification behaviors after this
			NotificationManager notificationManager = getSystemService(NotificationManager.class);
			notificationManager.createNotificationChannel(channel);
		}
	}

	protected void unloginIfNecessary() {
		SharedPreferences sharedPref = getWisSchedSharedPreferences();
		if (sharedPref.getString("jwtToken", null) == null) {
			SharedPreferences.Editor editor = sharedPref.edit();
			editor.putBoolean("upgradedLogin", true);
			editor.remove("personId");
			editor.remove("appKey");
			editor.remove("jwtToken");
			editor.apply();
		}
	}
	
	public void login(String email, String password, LoginResult loginResult) {
		new LoginTask(this, email, password, loginResult).execute();
	}
	public void fetchAssignmentsIfNecessary() {
		if (this.assignmentsCheck < 0 || (System.currentTimeMillis() - this.assignmentsCheck) > THREE_HOURS) {
			fetchAssignments();
		}
	}
	public String getClientId() {
		SharedPreferences preferences = getWisSchedSharedPreferences();
		String clientId = preferences.getString("clientId", null);
		if (StringUtils.isEmpty(clientId)) {
			clientId = UUID.randomUUID().toString();
			SharedPreferences.Editor editor = preferences.edit();
			editor.putString("clientId", clientId);
			editor.apply();
		}
		return clientId;
	}
	public String getPersonId() {
		return getWisSchedSharedPreferences().getString("personId", null);
	}
	public boolean isLoggedIn() {
		return getWisSchedSharedPreferences().getString("jwtToken", null) != null;
	}

    protected void fetchAssignments() {
		String appKey = getWisSchedSharedPreferences().getString("appKey", null);
		if (appKey != null) {
			this.assignmentsCheck = System.currentTimeMillis();
			new AssignmentsTask(this).execute();
		}
	}

	private SharedPreferences getWisSchedSharedPreferences() {
		return getSharedPreferences("WisSched", Context.MODE_PRIVATE);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	public void addPropertyChangeListener(String eventType, PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(eventType, listener);
	}
	
	public RestaurantService getRestaurantService() {
		return restaurantService;
	}

	public void performDataSynchIfApplicable() {
		if (!ConnectivityUtil.isNetworkAvailable(this)) {
			// skip
		} else if (this.lastDataSynchTime < 0 || ((System.currentTimeMillis() - this.lastDataSynchTime) > SIXTY_MINUTES)) {
			long lastCheck = this.lastDataSynchTime;
			this.lastDataSynchTime = System.currentTimeMillis();

			this.eventService.uploadSelections();
			
			this.eventService.fetchLatestUpdates();
			
			this.eventService.fetchLatestRankings();
			
			if (lastCheck < 0 || ((System.currentTimeMillis() - lastCheck) > THREE_HOURS)) {
				this.announcementService.fetchLatestUpdates();
			}
			
			this.imageService.uploadQueuedImage();

            new AvatarDownloadTask(this.imageService, getRestService()).execute();
		}
	}
	
	public String getToken() {
		String temp = "" + getClientId() + "||" + TOKEN;
		if (StringUtils.isNotEmpty(getPersonId())) {
			temp += "||" + getPersonId();
		}
		
		try {
		    MessageDigest md = MessageDigest.getInstance("SHA-256");
	
		    md.update(temp.getBytes());
		    byte[] digest = md.digest();
	
		    return Base64.encodeToString(digest, Base64.NO_WRAP);		
		} catch (NoSuchAlgorithmException e) {
			return "";
		}
	}

	public ImageService getImageService() {
		return imageService;
	}

	public AnnouncementService getAnnouncementService() {
		return announcementService;
	}

	public ReferenceService getReferenceService() {
		return referenceService;
	}
}
