package org.ayizan.android.wissched.view;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;

import org.ayizan.android.wissched.view.event.EventDetailsActivity;
import org.ayizan.android.wissched.ItemOpener;
import org.ayizan.android.event.model.Event;

/**
 * Created by BC Holmes on 2016-08-18.
 */
public class EventOpener implements ItemOpener<Event> {

    public static final String ENTITY_ID = "ENTITY_ID";
    Context activity;

    public EventOpener(Context activity) {
        this.activity = activity;
    }

    @Override
    public void open(Event item) {
        Intent intent = createIntent(item);
        activity.startActivity(intent);

    }

    @NonNull
    public Intent createIntent(Event item) {
        Intent intent = new Intent(activity, EventDetailsActivity.class);
        return addEventExtras(item, intent);
    }

    @NonNull
    public Intent addEventExtras(Event item, Intent intent) {
        intent.putExtra(ENTITY_ID, item.getId());
        return intent;
    }
}
