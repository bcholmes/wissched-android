package org.ayizan.android.wissched;

import android.os.AsyncTask;
import android.util.Log;

import org.ayizan.android.event.BaseEventApplication;
import org.ayizan.android.event.model.Event;
import org.ayizan.android.event.service.EventService;
import org.ayizan.android.event.service.rest.AssignmentsResponse;
import org.ayizan.android.event.service.rest.RestResponse;
import org.ayizan.android.event.service.rest.RestService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class AssignmentsTask extends AsyncTask<String, Void, RestResponse<AssignmentsResponse>> {

	private final RestService restService;
	private EventService eventService;

	AssignmentsTask(WisSchedApplication wisSchedApplication) {
		this.restService = wisSchedApplication.getRestService();
		this.eventService = wisSchedApplication.getEventService();
	}
	
	@Override
	protected RestResponse<AssignmentsResponse> doInBackground(String... params) {
		Log.d(BaseEventApplication.TAG, "Fetching assignments");
		try {
			return this.restService.fetchAssignments();
		} catch (IOException e) {
			return null;
		}
	}

	protected void onPostExecute(RestResponse<AssignmentsResponse> result) {
		if (result != null && result.getStatusCode() == 200) {
			Log.d(BaseEventApplication.TAG, "Assignments successfully retrieved.");
			List<Event> events = new ArrayList<Event>();
			for (String eventId : result.getBody().getAssignments()) {
				Event event = this.eventService.getEventById(eventId);
				if (event != null) {
					event.setInSchedule(true);
					events.add(event);
				}
			}
			this.eventService.saveEvents(events);
		} else {
			Log.d(BaseEventApplication.TAG, "non-successful");
		}
	}
}