package org.ayizan.android.wissched.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

import org.ayizan.android.event.BaseEventApplication;
import org.ayizan.android.event.model.Groupable;
import org.ayizan.android.event.model.Renderable;
import org.ayizan.android.event.util.RenderContext;
import org.ayizan.android.event.util.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.util.Log;

public class Restaurant implements Groupable, Comparable<Restaurant>, Renderable {
	
	private String id;
	private String name;
	private String url;
	private String cuisine;
	private String streetAddress;
	private String fullAddress;
	private String notes;
	private String hours;
	private String memorialDayStatus;
	private String veggie;
	private String allergen;
	private String alcohol;
	private String takeout;
	private String phoneNumber;
	private Availability wifi;
	private Availability delivery;
	private int priceRange;
	private double latitude;
	private double longitude;
	private Availability reservationRecommended;

	public static Restaurant fromJson(JSONObject restaurantJson) throws JSONException {
		Restaurant result = new Restaurant();
		if (restaurantJson.has("id")) {
			result.id = restaurantJson.getString("id");
		}
		if (restaurantJson.has("name")) {
			result.name = restaurantJson.getString("name");
		}
		if (restaurantJson.has("url")) {
			result.url = restaurantJson.getString("url");
		}
		if (restaurantJson.has("hours")) {
			result.hours = restaurantJson.getString("hours");
		}
		if (restaurantJson.has("alcohol")) {
			result.alcohol = restaurantJson.getString("alcohol");
		}
		if (restaurantJson.has("cuisine")) {
			result.cuisine = restaurantJson.getString("cuisine");
		}
		if (restaurantJson.has("streetAddress")) {
			result.streetAddress = restaurantJson.getString("streetAddress");
		}
		if (restaurantJson.has("fullAddress")) {
			result.fullAddress = restaurantJson.getString("fullAddress");
		}
		if (restaurantJson.has("notes")) {
			result.notes = restaurantJson.getString("notes");
		}
		if (restaurantJson.has("memorialDayStatus")) {
			result.memorialDayStatus = restaurantJson.getString("memorialDayStatus");
		}
		if (restaurantJson.has("veggie")) {
			result.veggie = restaurantJson.getString("veggie");
		}
		if (restaurantJson.has("allergen")) {
			result.allergen = restaurantJson.getString("allergen");
		}
		if (restaurantJson.has("takeout")) {
			result.takeout = restaurantJson.getString("takeout");
		}
		if (restaurantJson.has("phoneNumber")) {
			result.phoneNumber = restaurantJson.getString("phoneNumber");
		}
		result.wifi = getAvailability(restaurantJson, "wifi");
		result.delivery = getAvailability(restaurantJson,"delivery");
		result.reservationRecommended = getAvailability(restaurantJson,"reservationRecommended");

		if (restaurantJson.has("priceRange")) {
			result.priceRange = restaurantJson.getInt("priceRange");
		}

		if (restaurantJson.has("coordinates")) {

			JSONObject coordinate = restaurantJson.getJSONObject("coordinates");
			if (coordinate.has("latitude")) {
				result.latitude = coordinate.getDouble("latitude");
			}
			if (coordinate.has("longitude")) {
				result.longitude = coordinate.getDouble("longitude");
			}
		}

		Log.i(BaseEventApplication.TAG, "Lat/Long " + result.getLatitude() + "/" + result.getLongitude());

		return result;
	}

	private static Availability getAvailability(JSONObject restaurantJson, String key) throws JSONException {
		if (!restaurantJson.has(key) || restaurantJson.isNull(key)) {
			return Availability.UNKNOWN;
		} else if ("true".equalsIgnoreCase(restaurantJson.getString(key))) {
			return Availability.YES;
		} else {
			return Availability.NO;
		}
	}

	public String getName() {
		return name == null ? "" : name;
	}

	public String getUrl() {
		return url;
	}

	public String getCuisine() {
		return cuisine;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public String getNotes() {
		return notes;
	}

	public String getHours() {
		return hours;
	}

	public String getMemorialDayStatus() {
		return memorialDayStatus;
	}

	public String getVeggie() {
		return veggie;
	}

	public String getAllergen() {
		return allergen;
	}

	public String getTakeout() {
		return takeout;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public int getPriceRange() {
		return priceRange;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	@SuppressLint("DefaultLocale")
	@Override
	public String getGroup() {
		if (StringUtils.isNotEmpty(getName())) {
			String firstCharacter = getName().toUpperCase(Locale.ENGLISH).substring(0, 1);
			return "ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(firstCharacter) >= 0 ? firstCharacter : "#";
		} else {
			return "?";
		}
	}

	public String getId() {
		return id;
	}
	public String toString() {
		return getName();
	}

	@Override
	public int compareTo(Restaurant another) {
		return getName().compareTo(another.getName());
	}

	@Override
	public String getFullHtml(RenderContext renderContext) {
		StringBuilder builder = new StringBuilder();
		builder.append("<html><head><title>").append(getName())
			.append("%@</title><link rel=\"stylesheet\" type=\"text/css\" href=\"./list.css\" />");
		builder.append("<meta name=\"format-detection\" content=\"telephone=no\">");
		builder.append("</head><body><div class=\"content\"><div class=\"main\">");
		builder.append("<p><b>").append(getName()).append("</b></p>");
		if (!StringUtils.isBlank(this.fullAddress)) {
			try {
				String mapUrl = String.format("geo:0,0?q=%s",URLEncoder.encode(this.fullAddress, "UTF-8"));
				builder.append("<div class=\"floatRight\"><a class=\"map-button\" href=\"").append(mapUrl).append("\">Map</a></div>");
			} catch (UnsupportedEncodingException e) {
			}
		}
		builder.append("<p>").append(getStreetAddress()).append("<br />");
		builder.append(getPhoneNumber()).append("</p>");
		
		builder.append("<p><b>Cuisine:</b> ").append(getCuisine()).append("<br />");
		builder.append("<b>Veggie entrees:</b> ").append(getVeggie()).append("<br />");
		builder.append("<b>Alcohol:</b> ").append(getAlcohol()).append("<br />");
		builder.append("<b>Allergen info:</b> ").append(getAllergen());
		builder.append("</p>");
		
		builder.append("<p><b>Take-out:</b> ").append(getTakeout()).append("<br />");
		builder.append("<b>Delivery:</b> ").append(getDelivery());
		builder.append("</p>");
		
		builder.append("<p><b>Wifi:</b> ").append(getWifi());
		
		builder.append("<p><b>Hours:</b> ").append(getHours()).append("<br />");
		builder.append("<b>Open Memorial Day?</b> ").append(getMemorialDayStatus()).append("<br />");
		builder.append("<b>URL:</b> ").append(getUrl());
		builder.append("</p>");
		
		if (StringUtils.isNotEmpty(this.notes)) {
			builder.append("<p><b>Notes:</b><br />").append(this.notes).append("</p>");
		}
		
		if (getReservationRecommended() == Availability.YES) {
			builder.append("<p class=\"notice\">Reservations recommended</p>");
		}
		builder.append("</div></div></body></html>");
		return builder.toString();
	}

	@Override
	public String getFullTitleNoHtml() {
		return getName();
	}
	
	public String getAnnotations() {
		StringBuilder builder = new StringBuilder();
		
		if (!"No".equals(getTakeout())) {
			builder.append("Take-out");
		}
		if (getDelivery() == Availability.YES) {
			if (builder.length() > 0) {
				builder.append(", ");
			}
			builder.append("Delivery");
		}
		if (!StringUtils.isBlank(getVeggie()) && !"no".equalsIgnoreCase(getVeggie())) {
			if (builder.length() > 0) {
				builder.append(", ");
			}
			builder.append("Veggie (").append(getVeggie()).append(")");
		}
		if (!StringUtils.isBlank(getAlcohol()) && !"no".equalsIgnoreCase(getAlcohol())) {
			if (builder.length() > 0) {
				builder.append(", ");
			}
			builder.append(getAlcohol());
		}
		if (getWifi() == Availability.YES) {
			if (builder.length() > 0) {
				builder.append(", ");
			}
			builder.append("Wifi");
		}
		if (!StringUtils.isBlank(getAllergen())) {
			if (builder.length() > 0) {
				builder.append(", ");
			}
			builder.append(getAllergen());
		}
		
		return builder.toString();
	}

	public String getPriceRangeString() {
		if (this.priceRange <= 0) {
			return "";
		} else if (this.priceRange >= 4) {
			return "N/A";
		} else {
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < this.priceRange; i++) {
				builder.append("$");
			}
			return builder.toString();
		}
	}

	public String getAlcohol() {
		return alcohol;
	}

	public String getFullAddress() {
		return fullAddress;
	}

	public Availability getWifi() {
		return wifi;
	}

	public void setWifi(Availability wifi) {
		this.wifi = wifi;
	}

	public Availability getDelivery() {
		return delivery;
	}

	public void setDelivery(Availability delivery) {
		this.delivery = delivery;
	}

	public Availability getReservationRecommended() {
		return reservationRecommended;
	}

	public void setReservationRecommended(Availability reservationRecommended) {
		this.reservationRecommended = reservationRecommended;
	}

	public String getFullyQualifiedUrl() {
		if (this.url == null || this.url.toLowerCase().startsWith("http")) {
			return this.url;
		} else {
			return "http://" + this.url;
		}
	}

	public String getFullyQualifiedPhoneNumber() {
		if (this.phoneNumber == null || this.phoneNumber.length() > 10) {
			return this.phoneNumber;
		} else {
			return "608-" + this.phoneNumber;
		}
	}

	public boolean matchesString(String searchTerm) {
		if (searchTerm == null) {
			return false;
		} else if (this.name != null && this.name.toLowerCase().contains(searchTerm.toLowerCase())) {
			return true;
		} else if (this.cuisine != null && this.cuisine.toLowerCase().contains(searchTerm.toLowerCase())) {
			return true;
		} else if (getAnnotations() != null && getAnnotations().toLowerCase().contains(searchTerm.toLowerCase())) {
			return true;
		} else {
			return false;
		}
	}
}
