package org.ayizan.android.event.util;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class JSONResourceReader {
	
	public JSONObject loadJson(Context context, String resourceName) {
		try {
			return new JSONObject(readJson(context, resourceName));
		} catch (JSONException e) {
			return new JSONObject();
		}
	}

	public JSONArray loadJsonArray(Context context, String resourceName) {
		try {
			return new JSONArray(readJson(context, resourceName));
		} catch (JSONException e) {
			return new JSONArray();
		}
	}

	private String readJson(Context context, String resourceName) {
		try {
			InputStream input = context.getAssets().open(resourceName);
			try {
				return IOUtils.toString(input, "UTF-8");
			} finally {
				IOUtils.closeQuietly(input);
			}
		} catch (IOException e) {
			return "{}";
		}
	}
	

}
