package org.ayizan.android.event.service;

import org.json.JSONArray;
import org.json.JSONObject;

public class CacheItem {

	String lastModified;
	JSONObject json;
	JSONArray jsonArray;
	
	String toExternalForm() {
		String jsonString = (this.json != null) 
				? this.json.toString() 
				: (this.jsonArray != null ? this.jsonArray.toString() : "null");
		
		String jsonWithMetaData = this.lastModified == null 
				? "{ \"data\": " + jsonString + "}" 
				: "{ \"lastModified\": \"" + this.lastModified + "\", \"data\": " + jsonString + "}";
		return jsonWithMetaData;
	}
	
	CacheItem(String lastModified, JSONObject jsonObject) {
		this.lastModified = lastModified;
		this.json = jsonObject;
	}
	
	CacheItem(String lastModified, JSONArray jsonArray) {
		this.lastModified = lastModified;
		this.jsonArray = jsonArray;
	}

	public String getLastModified() {
		return lastModified;
	}

	public JSONObject getJson() {
		return json;
	}

	public JSONArray getJsonArray() {
		return jsonArray;
	}
	
}
