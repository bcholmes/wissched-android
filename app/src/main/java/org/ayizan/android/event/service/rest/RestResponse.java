package org.ayizan.android.event.service.rest;

import okhttp3.Response;

public class RestResponse<T> {
    int statusCode;
    String jwtToken;
    T body;

    String lastModifiedHeader;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getJwtToken() {
        return jwtToken;
    }

    public void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }

    public String getLastModifiedHeader() {
        return lastModifiedHeader;
    }

    @SuppressWarnings("unchecked")
    public static <T> RestResponse<T> createJsonError() {
        RestResponse result = new RestResponse();
        result.setStatusCode(500);
        return result;
    }

    @SuppressWarnings("unchecked")
    public static <T> RestResponse<T> create(Response response, T body) {
        RestResponse result = new RestResponse();
        result.body = body;
        result.statusCode = response.code();
        result.jwtToken = extractJwt(response.header("Authorization"));
        result.lastModifiedHeader = response.header("Last-Modified");
        return result;
    }

    private static String extractJwt(String authorization) {
        if (authorization == null) {
            return null;
        } else if (authorization.startsWith("Bearer ")) {
            return authorization.substring("Bearer ".length());
        } else {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> RestResponse<T> createError(Response response) {
        RestResponse result = new RestResponse();
        result.statusCode = response.code();
        result.jwtToken = extractJwt(response.header("Authorization"));
        return result;
    }

    @SuppressWarnings("unchecked")
    public static <T> RestResponse<T> createAuthenticationError() {
        RestResponse result = new RestResponse();
        result.statusCode = 401;
        return result;
    }
}
