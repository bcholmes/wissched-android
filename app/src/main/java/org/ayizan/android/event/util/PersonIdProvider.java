package org.ayizan.android.event.util;

public interface PersonIdProvider {
	public String getPersonId();
}
