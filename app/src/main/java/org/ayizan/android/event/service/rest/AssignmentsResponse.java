package org.ayizan.android.event.service.rest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AssignmentsResponse {

    List<String> assignments = new ArrayList<>();

    public List<String> getAssignments() {
        return assignments;
    }

    public static AssignmentsResponse fromJson(JSONObject json) throws JSONException {
        AssignmentsResponse result = new AssignmentsResponse();
        if (json.has("assignments")) {
            JSONArray array = json.getJSONArray("assignments");
            for (int i = 0; i < array.length(); i++) {
                result.assignments.add(array.getString(i));
            }
        }
        return result;
    }
}
