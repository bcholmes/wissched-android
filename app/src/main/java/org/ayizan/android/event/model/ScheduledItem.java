package org.ayizan.android.event.model;

import org.ayizan.android.wissched.view.grid.GridViewEvent;

public interface ScheduledItem {

    String getFullIdentifier();
    TimeRange getTimeRange();
    String getFullTitleNoHtml();
    Location getLocation();
    String getGridSubtitle();
}
