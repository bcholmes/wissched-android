package org.ayizan.android.event.service;

import android.os.AsyncTask;
import android.util.Log;

import org.ayizan.android.event.BaseEventApplication;
import org.ayizan.android.event.service.rest.FetchAvatarListResponse;
import org.ayizan.android.event.service.rest.RestResponse;
import org.ayizan.android.event.service.rest.RestService;

import java.io.IOException;

public class AvatarDownloadTask extends AsyncTask<String, Void, Void> {

	private ImageService imageService;
	private RestService restService;

	public AvatarDownloadTask(ImageService imageService, RestService restService) {
		this.imageService = imageService;
        this.restService = restService;
	}

	private void downloadAvatar(FetchAvatarListResponse.AvatarRecord record) {
		try {
			Log.d(BaseEventApplication.TAG, "Preparing fetch avatar for person " + record.getPersonId());
			RestResponse<byte[]> response = this.restService.fetchAvatar(record);

			if (response.getStatusCode() == 200) {
	        	saveAvatar(record.getPersonId(), response.getBody());
	        }
		} catch (IOException e) {
			Log.e(BaseEventApplication.TAG, "Unable to download avatar", e);
		}		
	}

	private void saveAvatar(String personId, byte[] data) throws IOException {
		this.imageService.saveAvatar(personId, data);
	}

	@Override
	protected Void doInBackground(String... params) {
        try {
            FetchAvatarListResponse response = this.restService.fetchAvatars();
            if (response != null) {
				for (FetchAvatarListResponse.AvatarRecord record : response.getRecords()) {
					downloadAvatar(record);
				}
			}
		} catch (IOException e) {
		}
		return null;
	}
}
