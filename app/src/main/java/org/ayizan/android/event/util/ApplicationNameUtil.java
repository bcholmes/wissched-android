package org.ayizan.android.event.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.util.Log;

import org.ayizan.android.event.BaseEventApplication;

public class ApplicationNameUtil {

    static String applicationName;

    public static void initialize(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        applicationName = stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
        Log.i(BaseEventApplication.TAG, "Application name: " + applicationName);
    }

    public static String getApplicationName() {
        return applicationName;
    }
}
