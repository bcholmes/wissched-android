package org.ayizan.android.event.util;

public interface ImageProvider {
	public boolean isImageAvailable();
	public String getImageSource();
	public boolean isAddImageAllowed();
}
