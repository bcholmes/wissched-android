package org.ayizan.android.event.db;

import org.ayizan.android.event.model.Event;

public interface EventFinder {
	public Event getEventById(String id);
}
