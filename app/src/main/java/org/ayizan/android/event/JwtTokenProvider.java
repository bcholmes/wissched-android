package org.ayizan.android.event;

public interface JwtTokenProvider {
    public String getJwtToken();
    public boolean isLoggedIn();
}
