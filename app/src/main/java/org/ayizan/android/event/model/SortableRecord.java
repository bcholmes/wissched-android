package org.ayizan.android.event.model;

/**
 * Created by BC Holmes on 16-08-21.
 */
public interface SortableRecord<T> extends Groupable, Comparable<T> {
}
