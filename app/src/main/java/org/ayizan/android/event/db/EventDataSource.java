package org.ayizan.android.event.db;

import org.ayizan.android.event.model.Event;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class EventDataSource {

	private DbHelper helper;
	private String currentConvention;

	public EventDataSource(Context context, String currentConvention, String databaseName) {
		this.currentConvention = currentConvention;
		this.helper = new DbHelper(context, databaseName);
	}

	public void saveEvent(Event event) {
		SQLiteDatabase database = helper.getWritableDatabase();
		try {
			if (isEventAlreadyInserted(event, database)) {
				Log.w("db", "already inserted");
				database.execSQL("update event set is_highlighted = ?, my_schedule = ?, yay = ?, omg = ?, rank_score = ?, "
						+ "yay_score = ?, omg_score = ? where event_id = " + event.getId(), 
						new Object[] { 
							event.isHighlighted() ? "Y" : "N", 
							event.isInSchedule() ? "Y" :"N", 
							event.isYay() ? "Y" :"N",
							event.isOmg() ? "Y" :"N",
							event.getRanking().getScore(), 
							event.getRanking().getYayCount(), 
							event.getRanking().getOmgCount() 
						});
			} else {
				Log.w("db", "not already inserted");
				database.execSQL("insert into event " +
						"( event_id, is_highlighted, my_schedule, rating_overall, rating_on_topic, "
						+ "yay, omg, rank_score, yay_score, omg_score, convention ) values ( " +
						event.getId() + ", ?, ?, 0, 0, ?, ?, ?, ?, ?, ? )", 
						new Object[] { event.isHighlighted() ? "Y" : "N", event.isInSchedule() ? "Y" :"N", 
								event.isYay() ? "Y" :"N",
								event.isOmg() ? "Y" :"N",
								event.getRanking().getScore(), 
								event.getRanking().getYayCount(), 
								event.getRanking().getOmgCount(),
								this.currentConvention });
			}
		} finally {
//			database.close();
		}
	}

	private boolean isEventAlreadyInserted(Event event, SQLiteDatabase database) {
		Cursor cursor = database.rawQuery("select count(*) from event where event_id = " + event.getId() 
				+ " and convention = '" + this.currentConvention + "'", null);
		try {
			int count = 0;
			cursor.moveToFirst(); 
			while (!cursor.isAfterLast()) {
				count = cursor.getInt(0);
				cursor.moveToNext();
			}
			return count > 0;
		} finally {
			cursor.close();
		}
	}

	public void readAllEvents(EventFinder eventFinder) {
		SQLiteDatabase database = this.helper.getReadableDatabase();
		try {
			Cursor cursor = database.rawQuery("select * from event", null);
			try {
				for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
					int id = cursor.getInt(cursor.getColumnIndex("event_id"));
					Event event = eventFinder.getEventById("" + id);
					if (event != null) {
						String highlightFlag = cursor.getString(cursor.getColumnIndex("is_highlighted"));
						event.setHighlighted("Y".equalsIgnoreCase(highlightFlag));
						
						String scheduleFlag = cursor.getString(cursor.getColumnIndex("my_schedule"));
						event.setInSchedule("Y".equalsIgnoreCase(scheduleFlag));
						
						String yayFlag = cursor.getString(cursor.getColumnIndex("yay"));
						event.setYay("Y".equalsIgnoreCase(yayFlag));
						
						String omgFlag = cursor.getString(cursor.getColumnIndex("omg"));
						event.setOmg("Y".equalsIgnoreCase(omgFlag));
						
						event.getRanking().setScore(cursor.getInt(cursor.getColumnIndex("rank_score")));
						event.getRanking().setYayCount(cursor.getInt(cursor.getColumnIndex("yay_score")));
						event.getRanking().setOmgCount(cursor.getInt(cursor.getColumnIndex("omg_score")));
						
						event.setTouched(true);
					}					
				}
			} finally {
				cursor.close();
			}
		} finally {
//			database.close();
		}
	}
}
