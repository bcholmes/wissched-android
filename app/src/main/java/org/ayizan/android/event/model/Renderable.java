package org.ayizan.android.event.model;

import org.ayizan.android.event.util.RenderContext;

public interface Renderable {
	public String getId();
	public String getFullHtml(RenderContext renderContext);
	public String getFullTitleNoHtml();
}
