package org.ayizan.android.event.service;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import org.ayizan.android.event.model.Participant;
import org.ayizan.android.event.service.rest.RestResponse;
import org.ayizan.android.event.service.rest.RestService;
import org.ayizan.android.event.util.ApplicationNameUtil;
import org.ayizan.android.event.util.ConnectivityUtil;
import org.ayizan.android.event.util.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.ayizan.android.event.BaseEventApplication.TAG;

public class ImageService {

	public class UploadAvatarTask extends AsyncTask<String, Void, Void> {

		private JSONObject json;
		private RestService restService;

		public UploadAvatarTask(JSONObject json, RestService restService) {
			this.json = json;
			this.restService = restService;
		}
		
		@Override
		protected Void doInBackground(String... params) {
			try {
				Log.d(TAG, "Preparing avatar upload request: " + json.toString());

				RestResponse<Void> response = this.restService.uploadAvatar(this.json);

				Log.i(TAG, "HTTP Avatar upload status " + response.getStatusCode());
				if (response.getStatusCode() == 200) {
					deleteUploadCache();
				}
		        return null;
			} catch (IOException e) {
				Log.e(TAG, "error sending request", e);
				return null;
			}
		}
	}

	Context context;
	private TokenGenerator tokenGenerator;
	private CacheService cacheService;
	private RestService restService;

	public ImageService(Context context, TokenGenerator tokenGenerator, RestService restService) {
		this.context = context;
		this.tokenGenerator = tokenGenerator;
		this.cacheService = new CacheService(context);
		this.restService = restService;
	}
	
	public void saveAvatar(String personId, byte[] byteArray) {
		
		File avatar = fileForPersonId(personId);
		
		try {
			FileOutputStream outputStream = new FileOutputStream(avatar);
			try {
				outputStream.write(byteArray);
			} finally {
				IOUtils.closeQuietly(outputStream);
			}
		} catch (IOException e) {
			Log.e(TAG, "Problem saving avatar", e);
		}
	}

	public File fileForParticipant(Participant participant) {
		return fileForPersonId(participant.getId());
	}
	private File fileForPersonId(String personId) {
		File dir = this.context.getDir("avatars", Context.MODE_PRIVATE);
		return new File(dir, ApplicationNameUtil.getApplicationName() + "_person" + personId);
	}

	public boolean isAvatarAvailable(Participant participant) {
		return fileForPersonId(participant.getId()).exists();
	}
	
	private String getDataType(byte[] imageData) {
		if (imageData == null || imageData.length == 0) {
			return null;
		} else {
			switch ((int) imageData[0]) {
			case 0xff:
				return "image/jpeg";
			case 0x89:
				return "image/png";
			case 0x47:
				return "image/gif";
			case 0x49:
			case 0x4D:
				return "image/tiff";
			default:
				return null;
			}
		}
	}

	private byte[] toByteArray(File avatar) throws IOException {
		FileInputStream input = new FileInputStream(avatar);
		try {
			return IOUtils.toByteArray(input);
		} finally {
			IOUtils.closeQuietly(input);
		}
	}

	public void writeImageToFileSystem(Bitmap bitmap, Participant participant) throws IOException {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 90, bytes);
		byte[] data = bytes.toByteArray();
		
		File file = fileForPersonId(participant.getId());
		FileOutputStream output = new FileOutputStream(file);
		try {
			output.write(data);
		} finally {
			IOUtils.closeQuietly(output);
		}
		prepareForUpload(data);
	}

	private void prepareForUpload(byte[] bytes) {
		try {
			JSONObject json = createUploadJson(bytes);
			this.cacheService.writeCachedData("avatarUpload", new CacheItem(null, json));
			upload(json);
		} catch (JSONException e) {
			Log.e(TAG, "Unable to create the avatar upload JSON", e);
		} catch (IOException e) {
			Log.e(TAG, "Unable to cache the avatar", e);
		}
	}

	public void uploadQueuedImage() {
		try {
			CacheItem item = this.cacheService.getCachedData("avatarUpload");
			if (item != null && item.getJson() != null) {
				upload(item.getJson());
			} else {
				Log.i(TAG, "No avatar to upload");
			}
		} catch (IOException e) {
			Log.e(TAG, "Problem with image cache", e);
		}
	}
	
	private void upload(JSONObject json) {
		if (ConnectivityUtil.isNetworkAvailable(this.context)) {
			new UploadAvatarTask(json, this.restService).execute();
		}
	}
	
	void deleteUploadCache() {
		this.cacheService.deleteKey("avatarUpload");
	}

	private JSONObject createUploadJson(byte[] bytes) throws JSONException {
		JSONObject result = new JSONObject();
		result.put("clientId", this.tokenGenerator.getClientId());
		result.put("personId", this.tokenGenerator.getPersonId());
		result.put("mimeType", "image/png");
		result.put("convention", ApplicationNameUtil.getApplicationName().toLowerCase());
		
		Log.d(TAG, result.toString());
		result.put("photo", Base64.encodeToString(bytes, Base64.NO_WRAP));
		return result;
	}
}
