package org.ayizan.android.event.model;

public class Participation {

	private Participant participant;
	private ParticipationType type = ParticipationType.PANELIST;
	private boolean cancelled;
	private boolean hidden;
	
	public Participant getParticipant() {
		return participant;
	}
	public void setParticipant(Participant participant) {
		this.participant = participant;
	}
	public ParticipationType getType() {
		return type;
	}
	public void setType(ParticipationType type) {
		this.type = type;
	}
	public boolean isCancelled() {
		return cancelled;
	}
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
	public boolean isModerator() {
		return this.type.isModerator();
	}
	public boolean isHidden() {
		return this.hidden;
	}
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
}
