package org.ayizan.android.event.service.rest;

import org.ayizan.android.event.model.Announcement;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

public class FetchAnnouncementsResponse {

    public List<Announcement> getAnnouncements() {
        return announcements;
    }

    private List<Announcement> announcements;

    public static FetchAnnouncementsResponse fromJson(JSONArray json) throws JSONException {
        FetchAnnouncementsResponse result = new FetchAnnouncementsResponse();
        result.announcements = Announcement.fromJsonCollection(json);
        return result;
    }
}
