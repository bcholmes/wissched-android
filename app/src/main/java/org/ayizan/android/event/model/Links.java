package org.ayizan.android.event.model;

import java.util.Set;

public interface Links {
    Set<Link> getLinks();
}
