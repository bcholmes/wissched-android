package org.ayizan.android.event.model;

public enum EntityType {

	EVENT, PARTICIPANT, RESTAURANT, ANNOUNCEMENT;
}
